package com.kenkuan.migoevent

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView
import com.kenkuan.migoevent.event.Event
import io.reactivex.Flowable
import io.reactivex.disposables.CompositeDisposable
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var listView: ListView
    private lateinit var app: App
    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        Realm.init(this.applicationContext)

        this.app = (application as MigoApplication).app

        fab.setOnClickListener { view ->
            showCreateEventActivity()
        }

        this.listView = findViewById(R.id.event_list)
        this.listView.adapter = EventListAdapter(this, app.getEvents())
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showCreateEventActivity() {
        val intent = Intent(this, CreateEventActivity::class.java)
        startActivity(intent)
    }

    class EventListAdapter(private val activity: Activity, private val events: Flowable<List<Event>>) : BaseAdapter() {

        private val disposable = CompositeDisposable()
        private var data: List<Event> = ArrayList()

        init {
            this.disposable.add(this.events.subscribe {
                this.data = it
                this.notifyDataSetChanged()
            })
        }

        override fun getView(index: Int, convertView: View?, container: ViewGroup?): View {
            val view: View = convertView ?: activity.layoutInflater.inflate(R.layout.event_item, container, false)
            val event = getItem(index) as Event
            view.findViewById<TextView>(R.id.event_item_title).text = event.title
            return view
        }

        override fun getItem(index: Int): Any {
            return this.data[index]
        }

        override fun getItemId(index: Int): Long {
            return this.data[index].id.hashCode().toLong()
        }

        override fun getCount(): Int {
            return this.data.count()
        }

    }
}
