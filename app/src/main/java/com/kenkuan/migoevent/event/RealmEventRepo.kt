package com.kenkuan.migoevent.event

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.realm.Realm
import io.realm.Sort


class RealmEventRepo(private val realm: Realm) : EventRepo {

    override fun getEvents(): Flowable<List<Event>> {
        return realm.where(RealmEvent::class.java)
            .sort(
                "order",
                Sort.ASCENDING,
                "createdAt",
                Sort.DESCENDING
            )
            .findAllAsync()
            .asFlowable()
            .map {
                it.map { managedEvent ->
                    realm.copyFromRealm(managedEvent)
                }
            }
    }

    override fun getEvent(id: String): Event? {
        return getEvent(id, this.realm)
    }

    private fun getEvent(id: String, realm: Realm): Event? {
        return realm.where(RealmEvent::class.java)
            .equalTo("id", id)
            .findFirst()
    }

    override fun createEvent(event: Event): Single<Event> {
        return Single.create { single ->
            try {
                event.validate()
                val realmEvent = event as RealmEvent
                var result: Event? = null
                realm.executeTransactionAsync({
                    val res = it.copyToRealm(realmEvent)
                    result = it.copyFromRealm(res)
                }, {
                    single.onSuccess(result!!)
                }, { err ->
                    single.onError(err)
                })
            } catch (err: Exception) {
                single.onError(err)
            }
        }
    }

    override fun editEvent(event: Event): Single<Event> {
        return Single.create { single ->
            try {
                event.validate()
                val realmEvent = event as RealmEvent
                var result: Event? = null
                realm.executeTransactionAsync({
                    val res = it.copyToRealmOrUpdate(realmEvent)
                    result = it.copyFromRealm(res)
                }, {
                    single.onSuccess(result!!)
                }, { err ->
                    single.onError(err)
                })
            } catch (err: Exception) {
                single.onError(err)
            }
        }
    }

    override fun deleteEvent(event: Event): Completable {
        return Completable.create {
            realm.executeTransactionAsync({
                val managedEvent = getEvent(event.id, it) as? RealmEvent
                managedEvent?.deleteFromRealm()
            }, {
                it.onComplete()
            }, { err ->
                it.onError(err)
            })
        }
    }

}
