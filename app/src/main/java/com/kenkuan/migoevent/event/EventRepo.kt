package com.kenkuan.migoevent.event

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

interface EventRepo {

    fun getEvents(): Flowable<List<Event>>
    fun getEvent(id: String): Event?

    fun createEvent(event: Event): Single<Event>
    fun editEvent(event: Event): Single<Event>
    fun deleteEvent(event: Event): Completable

}
