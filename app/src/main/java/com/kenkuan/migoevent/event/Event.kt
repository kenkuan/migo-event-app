package com.kenkuan.migoevent.event

import java.util.*

/*:
Title (text with max 50 chars), Description (text with max 1000 chars), Start Date/Time, End Date/Time and Category (currently are Personal, Business, Others, but may add more in the future).
 */

interface Event {

    enum class Category {
        PERSONAL,
        BUSINESS,
        OTHERS
    }

    val id: String
    var title: String
    var description: String?
    var startAt: Date
    var endAt: Date
    var order: Int
    var category: Category
    var createdAt: Date
    var updatedAt: Date

    fun validate()
}
