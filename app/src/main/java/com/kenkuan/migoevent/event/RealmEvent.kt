package com.kenkuan.migoevent.event

import com.kenkuan.migoevent.InvalidPropertyException
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class RealmEvent(
    @PrimaryKey override var id: String = UUID.randomUUID().toString(),
    override var title: String = "",
    override var description: String? = null,
    override var startAt: Date = Date(),
    override var endAt: Date = Date(),
    override var order: Int = 0,
    override var createdAt: Date = Date(),
    override var updatedAt: Date = Date(),
    category: Event.Category = Event.Category.PERSONAL
) : RealmObject(), Event {

    override fun validate() {
        if (this.title.length > 50) {
            throw InvalidPropertyException("title", "length > 50")
        }
        val desc = this.description
        if (desc != null && desc.length > 1000) {
            throw InvalidPropertyException("description", "length > 1000")
        }
    }

    override var category: Event.Category
        get() {
            return Event.Category.valueOf(this.categoryDescription)
        }
        set(newValue) {
            this.categoryDescription = newValue.name
        }

    var categoryDescription: String = Event.Category.PERSONAL.name

    init {
        this.category = category
    }

    constructor(
        title: String,
        description: String?,
        startAt: Date,
        endAt: Date,
        order: Int,
        category: Event.Category
    ) :
            this(
                UUID.randomUUID().toString(),
                title,
                description,
                startAt,
                endAt,
                order,
                Date(),
                Date(),
                category
            )

}
