package com.kenkuan.migoevent

import com.kenkuan.migoevent.event.Event
import com.kenkuan.migoevent.event.EventRepo
import io.reactivex.Flowable
import io.reactivex.Single

class App(val eventRepo: EventRepo) {

    fun getEvents(): Flowable<List<Event>> {
        return eventRepo.getEvents()
    }

    fun createEvent(event: Event): Single<Event> {
        return eventRepo.createEvent(event)
    }

}
