package com.kenkuan.migoevent

class InvalidPropertyException(val property: String, val reason: String) :
    Exception("$property $reason")
