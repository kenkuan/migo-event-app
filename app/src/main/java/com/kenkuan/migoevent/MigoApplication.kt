package com.kenkuan.migoevent

import android.app.Application
import com.kenkuan.migoevent.event.RealmEventRepo
import io.realm.Realm

class MigoApplication : Application() {

    lateinit var app: App

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        val eventRepo = RealmEventRepo(Realm.getDefaultInstance())
        this.app = App(eventRepo)
    }
}

