package com.kenkuan.migoevent

import android.app.DatePickerDialog
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.EditText
import com.kenkuan.migoevent.event.Event
import com.kenkuan.migoevent.event.RealmEvent
import io.reactivex.disposables.CompositeDisposable
import java.util.*

class CreateEventActivity : AppCompatActivity() {

    private lateinit var titleEditText: EditText
    private lateinit var descEditText: EditText
    private lateinit var startAtEditText: EditText
    private lateinit var endAtEditText: EditText
    private lateinit var app: App

    private var startAt: Date? = null
    private var endAt: Date? = null
    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.create_event_activity)

        this.app = (application as MigoApplication).app
        this.titleEditText = findViewById(R.id.event_form_title)
        this.descEditText = findViewById(R.id.event_form_desc)
        this.startAtEditText = findViewById(R.id.event_form_start_at_et)
        this.endAtEditText = findViewById(R.id.event_form_end_at_et)
    }


    fun pickStartAt(view: View) {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val date = calendar.get(Calendar.DAY_OF_MONTH)
        DatePickerDialog(view.context, { datePicker, year, month, date ->
            this.startAtEditText.setText("$year-$month-$date")
            calendar.set(year, month - 1, date)
            this.startAt = calendar.time
        }, year, month, date).show()
    }

    fun pickEndAt(view: View) {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val date = calendar.get(Calendar.DAY_OF_MONTH)
        DatePickerDialog(view.context, { datePicker, year, month, date ->
            this.endAtEditText.setText("$year-$month-$date")
            calendar.set(year, month - 1, date)
            this.endAt = calendar.time
        }, year, month, date).show()
    }

    fun createEvent(view: View) {
        val title = this.titleEditText.text.toString()
        val description = this.descEditText.text.toString()
        if (title.isEmpty()) {
            Snackbar.make(view, "Must input title", Snackbar.LENGTH_LONG)
                .setAction("Dismiss", null).show()
            return
        }
        if (this.startAt == null) {
            Snackbar.make(view, "Must select start date", Snackbar.LENGTH_LONG)
                .setAction("Dismiss", null).show()
            return
        }
        if (this.endAt == null) {
            Snackbar.make(view, "Must select end date", Snackbar.LENGTH_LONG)
                .setAction("Dismiss", null).show()
            return
        }
        val event = RealmEvent(
            title,
            description,
            this.startAt!!,
            this.endAt!!,
            0,
            Event.Category.PERSONAL
        )
        this.disposable.add(this.app.createEvent(event)
            .doOnError { err ->
                Snackbar.make(view, "Create event failed: ${err.message}", Snackbar.LENGTH_LONG)
                    .setAction("Dismiss", null).show()
            }.subscribe { _ ->
                finish()
            }
        )
    }
}
