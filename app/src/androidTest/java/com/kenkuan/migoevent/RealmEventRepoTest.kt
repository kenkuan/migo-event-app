package com.kenkuan.migoevent

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.kenkuan.migoevent.event.Event
import com.kenkuan.migoevent.event.RealmEvent
import com.kenkuan.migoevent.event.RealmEventRepo
import io.reactivex.disposables.CompositeDisposable
import io.realm.Realm
import io.realm.RealmConfiguration
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.time.Instant
import java.util.*
import java.util.concurrent.CountDownLatch
import kotlin.collections.ArrayList

@RunWith(AndroidJUnit4::class)
class RealmEventRepoTest {
    companion object {
        init {
            Realm.init(InstrumentationRegistry.getTargetContext())
        }
    }

    lateinit var realm: Realm
    lateinit var repo: RealmEventRepo
    lateinit var disposable: CompositeDisposable

    @Before
    fun beforeEach() {
        this.disposable = CompositeDisposable()
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            val testConfig = RealmConfiguration.Builder().inMemory().name("test-realm").build()
            this.realm = Realm.getInstance(testConfig)
            this.repo = RealmEventRepo(realm)
        }
    }

    @After
    fun afterEach() {
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            this.disposable.dispose()
            this.realm.close()
        }
    }

    @Test
    fun testCreateEvent() {
        val startAt = Date()
        val endAt = Date()
        val event = RealmEvent(
            "hi",
            "desc",
            startAt,
            endAt,
            1,
            Event.Category.BUSINESS
        )

        val newEvent: Event? = createEvent(event)

        Assert.assertEquals(newEvent?.id, event.id)
        Assert.assertEquals(newEvent?.title, "hi")
        Assert.assertEquals(newEvent?.description, "desc")
        Assert.assertEquals(newEvent?.startAt, startAt)
        Assert.assertEquals(newEvent?.endAt, endAt)
        Assert.assertEquals(newEvent?.order, 1)
        Assert.assertEquals(newEvent?.category, Event.Category.BUSINESS)
        Assert.assertEquals(newEvent?.createdAt, event.createdAt)
        Assert.assertEquals(newEvent?.updatedAt, event.updatedAt)
    }

    @Test
    fun testCreateEvent_titleTooLong() {
        val startAt = Date()
        val endAt = Date()
        val event = RealmEvent(
            "hi",
            "desc",
            startAt,
            endAt,
            1,
            Event.Category.BUSINESS
        )
        event.title = "n".repeat(51)

        try {
            createEvent(event)
            Assert.fail("should throw exception")
        } catch (err: InvalidPropertyException) {
            Assert.assertEquals(err.property, "title")
            Assert.assertEquals(err.message, "title length > 50")
        }
    }

    @Test
    fun testCreateEvent_descTooLong() {
        val startAt = Date()
        val endAt = Date()
        val event = RealmEvent(
            "hi",
            "desc",
            startAt,
            endAt,
            1,
            Event.Category.BUSINESS
        )
        event.description = "n".repeat(1001)

        try {
            createEvent(event)
            Assert.fail("should throw exception")
        } catch (err: InvalidPropertyException) {
            Assert.assertEquals(err.property, "description")
            Assert.assertEquals(err.message, "description length > 1000")
        }
    }

    private fun createEvent(event: RealmEvent): Event? {
        val countDownLatch = CountDownLatch(1)
        var newEvent: Event? = null
        var throwable: Throwable? = null
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            this.disposable.add(this.repo.createEvent(event)
                .subscribe { result, err ->
                    newEvent = result
                    throwable = err
                    countDownLatch.countDown()
                }
            )
        }
        countDownLatch.await()
        if (throwable != null) {
            throw throwable!!
        }
        return newEvent
    }

    @Test
    fun testUpdateEvent() {
        val startAt = Date()
        val endAt = Date()
        val event = RealmEvent(
            "hi",
            "desc",
            startAt,
            endAt,
            1,
            Event.Category.BUSINESS
        )

        createEvent(event)

        event.title = "gg"
        event.description = "haha123"

        val newEvent: Event? = editEvent(event)

        Assert.assertEquals(newEvent?.id, event.id)
        Assert.assertEquals(newEvent?.title, "gg")
        Assert.assertEquals(newEvent?.description, "haha123")
        Assert.assertEquals(newEvent?.startAt, startAt)
        Assert.assertEquals(newEvent?.endAt, endAt)
        Assert.assertEquals(newEvent?.category, Event.Category.BUSINESS)
        Assert.assertEquals(newEvent?.createdAt, event.createdAt)
        Assert.assertEquals(newEvent?.updatedAt, event.updatedAt)
    }

    @Test
    fun testUpdateEvent_titleTooLong() {
        val startAt = Date()
        val endAt = Date()
        val event = RealmEvent(
            "hi",
            "desc",
            startAt,
            endAt,
            1,
            Event.Category.BUSINESS
        )

        createEvent(event)

        event.title = "g".repeat(51)

        try {
            editEvent(event)
            Assert.fail("should throw exception")
        } catch (err: InvalidPropertyException) {
            Assert.assertEquals(err.property, "title")
            Assert.assertEquals(err.message, "title length > 50")
        }
    }

    @Test
    fun testUpdateEvent_descTooLong() {
        val startAt = Date()
        val endAt = Date()
        val event = RealmEvent(
            "hi",
            "desc",
            startAt,
            endAt,
            1,
            Event.Category.BUSINESS
        )

        createEvent(event)

        event.description = "g".repeat(1001)

        try {
            editEvent(event)
            Assert.fail("should throw exception")
        } catch (err: InvalidPropertyException) {
            Assert.assertEquals(err.property, "description")
            Assert.assertEquals(err.message, "description length > 1000")
        }
    }

    private fun editEvent(event: RealmEvent): Event? {
        val countDownLatch = CountDownLatch(1)
        var newEvent: Event? = null
        var throwable: Throwable? = null
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            this.disposable.add(this.repo.editEvent(event)
                .subscribe { result, err ->
                    newEvent = result
                    throwable = err
                    countDownLatch.countDown()
                }
            )
        }
        countDownLatch.await()

        if (throwable != null) {
            throw throwable!!
        }
        return newEvent
    }

    @Test
    fun testDeleteEvent() {
        val startAt = Date()
        val endAt = Date()
        val event = RealmEvent(
            "hi",
            "desc",
            startAt,
            endAt,
            1,
            Event.Category.BUSINESS
        )

        createEvent(event)
        deleteEvent(event)

        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            val fetched = this.repo.getEvent(event.id)
            Assert.assertNull(fetched)
        }
    }

    private fun deleteEvent(event: RealmEvent) {
        val countDownLatch = CountDownLatch(1)
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            this.disposable.add(this.repo.deleteEvent(event)
                .subscribe {
                    countDownLatch.countDown()
                }
            )
        }
        countDownLatch.await()
    }

    @Test
    fun testGetEvents() {
        for (i in 1..10) {
            val startAt = Date.from(Instant.ofEpochSecond(i.toLong()))
            val endAt = Date.from(Instant.ofEpochSecond((i + 1000).toLong()))
            val event = RealmEvent(
                "hi $i",
                "desc $i",
                startAt,
                endAt,
                i,
                Event.Category.BUSINESS
            )
            createEvent(event)
        }

        val events = getEvents()
        Assert.assertEquals(events.count(), 10)
    }

    private fun getEvents(): List<Event> {
        val countDownLatch = CountDownLatch(1)
        var events: List<Event> = ArrayList()
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            this.disposable.add(this.repo.getEvents()
                .subscribe { result ->
                    events = result
                    countDownLatch.countDown()
                }
            )
        }
        countDownLatch.await()
        return events
    }
}
